#!/bin/sh
cat << EOF
version: "3"

networks:
  proxy:
    external: true

services:
  green-deploy:
    image: phaus/service-deployment:latest
    restart: always
    environment:
      - DEPLOYMENT=green
    labels:
      - traefik.backend=$USER-deploy
      - traefik.frontend.rule=Host:$USER.a19.ws.innoq.dev
      - traefik.docker.network=proxy
      - traefik.port=9000
    networks:
      - proxy

  blue-deploy:
    image: phaus/service-deployment:latest
    restart: always
    environment:
      - DEPLOYMENT=blue
    labels:
      - traefik.backend=$USER-deploy
      - traefik.frontend.rule=Host:$USER.a19.ws.innoq.dev
      - traefik.docker.network=proxy
      - traefik.port=9000
    networks:
      - proxy
EOF