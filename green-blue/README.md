# Green-Blue Deployment

## Preparation

Because with don't want to collide with differen URLs, we need to generate a deployment file with a user spefice URL first:

```bash
./prepare-docker-compose.sh > docker-compose.yml
```

## Deploying

We can now proceed with the deployment.

```bash
docker-compose -p $USER up -d  green-deploy
```

This starts the deployment of the service as the green deployment.

Go to `https://$USER.a19.ws.innoq.dev` and check the deployment. Reload several times to confirm, that only the green deployment is running.

## Canary deployment

Lets perform our canary deployment and deploy the new `blue` deployment:

```bash
docker-compose -p $USER up -d  blue-deploy
```

Hitting `https://$USER.a19.ws.innoq.dev` again several times, should now change between green and blue.


## Scaling

Scale up blue (3:1) => 75% of all requests go to blue

```bash
docker-compose -p $USER scale blue-deploy=3
```

Scale down blue (1:1) => 50% of all requests go to blue

```bash
docker-compose -p $USER scale blue-deploy=1
```

Scale down green (1:0) => 100% of all requests go to blue

```bash
docker-compose -p $USER scale green-deploy=0
```

## Undeploying

```bash
docker-compose -p $USER down
```
