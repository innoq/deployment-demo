# Green-Blue Deployment

## Deploying

```bash
docker-compose -p $USER up -d
```

## Undeploying

```bash
docker-compose -p $USER down
```
