#!/bin/sh
cat << EOF
version: "3"

networks:
  proxy:
    external: true

services:
  $USER-error-service:
    image: phaus/error-service:latest
    restart: always
    labels:
      - traefik.backend=$USER-error-service
      - traefik.frontend.rule=Host:$USER-error.a19.ws.innoq.dev
      - traefik.docker.network=proxy
      - traefik.port=9000
    networks:
      - proxy
EOF